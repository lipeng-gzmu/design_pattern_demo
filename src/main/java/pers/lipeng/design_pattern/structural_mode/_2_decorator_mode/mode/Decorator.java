package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.mode;

import java.util.logging.Logger;

/**
 * 抽象装饰器
 */
abstract class Decorator extends Component{
    public Component component;


    public Decorator(Component component){
        this.component = component;
    }

    /**
     * 将执行动作转发给组件本身执行，可在执行前后加其他的逻辑
     */
    @Override
    public void excute() {
        this.component.excute();
    }
}
