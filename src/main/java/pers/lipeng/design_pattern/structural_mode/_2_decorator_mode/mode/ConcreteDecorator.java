package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.mode;

/**
 * 具体装饰器
 */
class ConcreteDecorator extends Decorator{
    public ConcreteDecorator(Component component){
        super(component);
    }
    public void before(){
        System.out.println("逻辑之前");
    }
    public void after(){
        System.out.println("逻辑之后");
    }
    @Override
    public void excute() {
        before();
        component.excute();
        after();
    }
}
