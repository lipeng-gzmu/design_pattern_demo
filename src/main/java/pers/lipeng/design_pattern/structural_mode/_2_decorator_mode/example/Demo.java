package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.example;

class Demo {
    public final static Logger LOGGER = new Logger();
    public final static JsonLogger JSON_LOGGER = new JsonLogger(new Logger());
    public final static MapLogger MAP_LOGGER = new MapLogger(new Logger());
    public static void main(String[] args) {
        LOGGER.info("消息1");
        LOGGER.error("消息2");

        JSON_LOGGER.info("消息1");
        JSON_LOGGER.error("消息2");

        MAP_LOGGER.info("消息1");
        MAP_LOGGER.error("消息2");
    }
}
