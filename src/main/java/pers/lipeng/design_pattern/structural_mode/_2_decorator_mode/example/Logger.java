package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.example;

/**
 * 日志打印
 * 日志打印只会打印简单的格式，现在我们需要将日志
 * 转换为JSON数据格式打印
 */
class Logger {
    public void info(String msg){
        System.out.println("info:"+msg);
    }

    public void error(String msg){
        System.out.println("error"+msg);
    }
}
