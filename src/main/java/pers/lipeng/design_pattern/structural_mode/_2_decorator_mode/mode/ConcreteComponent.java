package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.mode;

/**
 * 具体组件
 */
class ConcreteComponent extends Component{
    @Override
    public void excute() {
        System.out.println("业务逻辑...");
    }
}
