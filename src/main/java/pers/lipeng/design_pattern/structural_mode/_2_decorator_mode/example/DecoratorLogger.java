package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.example;

/**
 * 抽象装饰器
 */
abstract class DecoratorLogger extends Logger{
    private Logger logger;

    public DecoratorLogger(Logger logger){
        this.logger = logger;
    }

    @Override
    public void info(String msg) {
        this.logger.info(msg);
    }

    @Override
    public void error(String msg) {
        this.logger.error(msg);
    }
}
