package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.example;

import java.util.HashMap;
import java.util.Map;

/**
 * 具体装饰器
 * 将日志转换为键值对
 */
class MapLogger extends DecoratorLogger{
    public MapLogger(Logger logger){
        super(logger);
    }

    @Override
    public void info(String msg) {
        Map<String,String> map = new HashMap<>(10);
        map.put("message",msg);
        super.info(map.toString());
    }

    @Override
    public void error(String msg) {
        Map<String,String> map = new HashMap<>(10);
        map.put("message",msg);
        super.error(map.toString());
    }
}
