package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.example;

import java.util.HashMap;
import java.util.Map;

/**
 * 具体装饰器
 * 将日志转换为json
 */
class JsonLogger extends  DecoratorLogger{
    public JsonLogger(Logger logger){
        super(logger);
    }

    @Override
    public void info(String msg) {
        super.info(msg+"我被转成json了");
    }

    @Override
    public void error(String msg) {
        super.error(msg+"我被转成json了");
    }
}
