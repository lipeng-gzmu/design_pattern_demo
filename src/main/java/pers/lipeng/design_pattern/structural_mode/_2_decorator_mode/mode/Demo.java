package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.mode;

class Demo {
    public static void main(String[] args) {
        Component component = new ConcreteComponent();
        //给对象增加功能
        Decorator decorator = new ConcreteDecorator(component);
        decorator.excute();
    }
}
