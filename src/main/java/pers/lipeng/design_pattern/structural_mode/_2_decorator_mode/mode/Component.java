package pers.lipeng.design_pattern.structural_mode._2_decorator_mode.mode;

/**
 * 抽象组件
 */
abstract class Component {
    public abstract void excute();
}
