package pers.lipeng.design_pattern.structural_mode._7_flyweight_mode;

/**
 * 具体享元角色
 */
class AodiCar extends Car{
    @Override
    public String getCar() {
        return "奥迪汽车";
    }
}
