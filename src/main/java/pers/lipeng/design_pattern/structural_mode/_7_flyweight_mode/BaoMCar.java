package pers.lipeng.design_pattern.structural_mode._7_flyweight_mode;

/**
 * 具体享元角色
 */
class BaoMCar extends Car{
    @Override
    public String getCar() {
        return "宝马汽车";
    }
}
