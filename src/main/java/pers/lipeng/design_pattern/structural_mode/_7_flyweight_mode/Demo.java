package pers.lipeng.design_pattern.structural_mode._7_flyweight_mode;

class Demo {
    public static void main(String[] args) {
        CarFactory instance = CarFactory.getInstance();
        Car car = instance.getCar("1");
        car.show("红色");
        System.out.println("=============");
        car.show("绿色");
        System.out.println("=============");

        Car car2 = instance.getCar("2");
        car2.show("黑色");
        System.out.println("=============");
        car2.show("白色");
    }
}
