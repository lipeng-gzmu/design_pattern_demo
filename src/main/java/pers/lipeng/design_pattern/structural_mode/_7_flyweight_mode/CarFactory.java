package pers.lipeng.design_pattern.structural_mode._7_flyweight_mode;

import java.util.HashMap;

class CarFactory {
    private static CarFactory carFactory = new CarFactory();
    /**
     * 将内部状态缓存起来
     * 方便复用
     */
    private HashMap<String,Car> cars;
    private CarFactory(){
        cars = new HashMap<String,Car>();
        cars.put("1",new BaoMCar());
        cars.put("2",new AodiCar());
    }

    public static CarFactory getInstance(){
        return carFactory;
    }

    public Car getCar(String key){
        return cars.get(key);
    }


}
