package pers.lipeng.design_pattern.structural_mode._7_flyweight_mode;

/**
 * 抽象享元角色
 * 汽车
 */
abstract class Car {
    public abstract String getCar();


    public void show(String color){
        System.out.println("汽车颜色：" + color);
        System.out.println("汽车型号：" + this.getCar());
    }
}
