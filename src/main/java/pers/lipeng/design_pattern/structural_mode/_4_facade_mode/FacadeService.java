package pers.lipeng.design_pattern.structural_mode._4_facade_mode;

/**
 * 外观模式
 * 提供给客户端一个发送消息接口
 * 但是内部已经实现了发送短信、邮件、微信等复杂的业务逻辑
 */
class FacadeService {
    private AliyunSmsService aliyunSmsService;
    private MailSendService mailSendService;
    private WexinSendService wexinSendService;
    public FacadeService() {
        this.aliyunSmsService = new AliyunSmsService();
        this.mailSendService = new MailSendService();
        this.wexinSendService = new WexinSendService();
    }

    public void sendMsg(){
        aliyunSmsService.sendSms();
        mailSendService.sendMail();
        wexinSendService.sendWexin();
    }
}
