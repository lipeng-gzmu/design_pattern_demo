package pers.lipeng.design_pattern.structural_mode._4_facade_mode;

class Demo {
    public static void main(String[] args) {
        FacadeService facadeService = new FacadeService();
        facadeService.sendMsg();
    }
}
