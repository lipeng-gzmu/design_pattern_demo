package pers.lipeng.design_pattern.structural_mode._6_combination_mode;


/**
 * 叶子构件
 */
class ImageFile extends AbstractFile{
    private String name;


    public ImageFile(String name) {
        this.name = name;
    }

    public void ls(String var) {
        System.out.println(var+"ImageFile: " + name);
    }
}
