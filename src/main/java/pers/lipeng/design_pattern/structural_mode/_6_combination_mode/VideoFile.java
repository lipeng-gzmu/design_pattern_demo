package pers.lipeng.design_pattern.structural_mode._6_combination_mode;


/**
 * 叶子构件
 */
class VideoFile extends AbstractFile{
    private String name;

    public VideoFile(String name) {
        this.name = name;
    }

    @Override
    public void ls(String var) {
        System.out.println(var+"VideoFile: " + name);
    }
}
