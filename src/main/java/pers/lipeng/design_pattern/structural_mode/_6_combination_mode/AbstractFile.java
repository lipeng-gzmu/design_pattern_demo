package pers.lipeng.design_pattern.structural_mode._6_combination_mode;

/**
 * 抽象构件
 * 每个方法都抛出异常
 * 为什么要这样做？
 * 因为在组合模式中，叶子节点和容器节点都继承了抽象构件，
 * 但是叶子节点不可能有add、remove、getChild方法，
 * 如果不抛出异常，那么叶子节点就必须实现这些方法，
 */
abstract class AbstractFile {
    public void add(AbstractFile file){
        throw new UnsupportedOperationException();
    }

    public void remove(AbstractFile file){
        throw new UnsupportedOperationException();
    }

    public AbstractFile getChild(int i){
        throw new UnsupportedOperationException();
    }

    public void ls(String var){
        throw new UnsupportedOperationException();
    }
}
