package pers.lipeng.design_pattern.structural_mode._6_combination_mode;

import java.util.ArrayList;
import java.util.List;

/**
 * 容器构件
 */
class Folder extends AbstractFile{
    private String name;
    private List<AbstractFile> files = new ArrayList<>();

    public Folder(String name) {
        this.name = name;
    }
    @Override
    public void add(AbstractFile file) {
        files.add(file);
    }

    @Override
    public void remove(AbstractFile file) {
        files.remove(file);
    }

    @Override
    public AbstractFile getChild(int i) {
        return files.get(i);
    }

    @Override
    public void ls(String var) {
        System.out.println(var+":"+name);
        for (AbstractFile file : files) {
            file.ls(var+"------");
        }
    }
}
