package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._1_static_proxy_mode;

class Demo {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        UserDaoProxy userDaoProxy = new UserDaoProxy(userDao);
        userDaoProxy.login();
    }
}
