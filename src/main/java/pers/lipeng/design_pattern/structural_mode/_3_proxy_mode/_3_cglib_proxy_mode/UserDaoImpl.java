package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._3_cglib_proxy_mode;

class UserDaoImpl implements UserDao{
    @Override
    public boolean save(boolean isSuccess) {
        System.out.println("保存数据");
        return isSuccess;

    }
}
