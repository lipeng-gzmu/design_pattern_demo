package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._3_cglib_proxy_mode;

interface UserDao {
    boolean save(boolean isSuccess);
}
