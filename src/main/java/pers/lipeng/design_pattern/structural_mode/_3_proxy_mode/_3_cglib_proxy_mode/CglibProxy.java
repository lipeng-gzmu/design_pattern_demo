package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._3_cglib_proxy_mode;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Method;

/**
 * Cglib动态代理
 * 1. 需要引入cglib的jar文件
 * 2. 引入cglib-3.3.0.jar.idea在在本包中的
 * cglib-3.3.0.jar右单机选择Add as Library
 * 因为我们为手动使用cglib因此还需要引入asm.jar否则会报ClassNotFound
 * 引入本包中asm-9.6-20230626.174013-7.jar
 * 或者直接在pom.xml文件中添加依赖，此方式不需要手动引入asm.jar
 * 右单击选择Add as Library
 * 本代理实现简单事务的控制
 */
class CglibProxy implements MethodInterceptor {
    private Object target;
    public Object getInstance(Object target){
        //设置需要创建的子类
        this.target = target;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("开启事务");
        Object invoke = methodProxy.invoke(target, objects);
        if((boolean) invoke){
            System.out.println("提交事务");
        }else {
            System.out.println("回滚事务");
        }
        System.out.println("关闭事务");
        return invoke;
    }
}
