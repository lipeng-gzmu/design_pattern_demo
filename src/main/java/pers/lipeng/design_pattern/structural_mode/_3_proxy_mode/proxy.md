# 代理模式

+ 通过代理控制对象的访问，可以在这个对象调用方法之前、调用方法之后去处理/添加新的功能。 (也就是AO的P微实现) 
+ 代理在原有代码乃至原业务流程都不修改的情况下，直接在业务流程中切入新代码，增加新功能， 这也和Spring的（面向切面编程）很相似





使用：

+ Spring AOP
+ 日志打印
+ 异常处理
+ 事务控制
+ 权限控制



**分类：**

+ 静态代理

  静态定义代理类，简单代理模式，是动态代理的理论基础。常见使用在代理模式。由程序员创建或工具生成代理类的源码，再编译代理类。所谓静态也就是在程序运行前就已经存在 代理类的字节码文件，代理类和委托类的关系在运行前就确定了。

  + 缺点：每个需要代理的对象都需要自己重复编写代理，很不舒服
  + 优点：但是可以面相实际对象或者是接口的方式实现代理

+ 动态代理

  动态生成代理类，也称为Jdk自带动态代理；使用反射完成代理。需要有顶层接口才能使用，常见是mybatis的mapper文件是代 理。动态代理也叫做，JDK代理、接口代理。动态代理的对象，是利用JDK的API，动态的在内存中构建代理对象（是根据被代理的接口来动态 生成代理类的class文件，并加载运行的过程），这就叫动态代理

  + 缺点：必须是面向接口，目标业务类必须实现接口 
  + 优点：不用关心代理类，只需要在运行阶段才指定代理哪一个对象

+ Cglib动态代理

  也是使用反射完成代理，可以直接代理类（jdk动态代理不行），使用字节码技 术，不能对 final类进行继承。（需要导入jar包）CGLIB动态代理和jdk代理一样，使用反射完成代理，不同的是他可以直接代理类（jdk动态代理不 行，他必须目标业务类必须实现接口），CGLIB动态代理底层使用字节码技术，CGLIB动态代理不 能对 final类进行继承。（CGLIB动态代理需要导入jar包）。利用asm开源包，对代理对象类的class文件加载进来，通过修改其字节码生成子类来处理。