package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._2_dynamic_proxy_mode;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代理类
 * 可重复使用，不像静态代理那样要自己重复编写代理
 * 缺点：必须是面向接口，目标业务类必须实现接口
 * 优点：不用关心代理类，只需要在运行阶段才指定代理哪一个对象
 */
class InvocationHandleImpl implements InvocationHandler {
    /**
     * 被代理的对象
     */
    private Object target;

    public InvocationHandleImpl(Object target){
        this.target = target;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("调用前的操作");
        //以反射的方式创建对象。第一个为要创建的对象，第二个为方法的的参数
        Object invoke = method.invoke(target, args);
        System.out.println("调用后的操作");
        return invoke;
    }
}
