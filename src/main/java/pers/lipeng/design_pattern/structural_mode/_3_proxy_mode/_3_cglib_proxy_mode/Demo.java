package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._3_cglib_proxy_mode;

class Demo {
    public static void main(String[] args) {
        CglibProxy cglibProxy = new CglibProxy();
        UserDao userDao = (UserDao)cglibProxy.getInstance(new UserDaoImpl());

        userDao.save(true);

        System.out.println("=====================================");

        userDao.save(false);
    }
}
