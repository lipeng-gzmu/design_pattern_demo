package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._2_dynamic_proxy_mode;

class UserDaoImpl implements UserDao{
    @Override
    public void login() {
        System.out.println("用户登录了");
    }
}
