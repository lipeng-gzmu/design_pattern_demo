package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._2_dynamic_proxy_mode;

interface UserDao {
    void login();
}
