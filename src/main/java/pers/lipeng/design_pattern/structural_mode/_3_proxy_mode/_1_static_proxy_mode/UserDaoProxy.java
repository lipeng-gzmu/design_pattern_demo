package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._1_static_proxy_mode;

/**
 * 代理类
 * 对用户登录的前后进行操作
 * 缺点：每个需要代理的对象都需要自己重复编写代理，很不舒服，
 * 优点：但是可以面相实际对象或者是接口的方式实现代理
 */
class UserDaoProxy extends UserDao {
    private UserDao userDao;
    public UserDaoProxy(UserDao userDao){
        this.userDao = userDao;
    }
    @Override
    public void login() {
        System.out.println("用户登录前的操作");
        userDao.login();
        System.out.println("用户登录后的操作");
    }
}
