package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._1_static_proxy_mode;

/**
 * 被代理类
 */
class UserDao {
    public void login(){
        System.out.println("用户登录了");
    }
}
