package pers.lipeng.design_pattern.structural_mode._3_proxy_mode._2_dynamic_proxy_mode;

import java.lang.reflect.Proxy;

class Demo {
    public static void main(String[] args) {
        UserDao userDaoImpl = new UserDaoImpl();
        InvocationHandleImpl invocationHandle = new InvocationHandleImpl(userDaoImpl);

        //类加载器
        ClassLoader loader = userDaoImpl.getClass().getClassLoader();
        Class<?>[] interfaces = userDaoImpl.getClass().getInterfaces();

          //主要装载器、一组接口及调用处理动态代理实例
        UserDao userDao = (UserDao)Proxy.newProxyInstance(loader, interfaces, invocationHandle);
        userDao.login();
    }
}
