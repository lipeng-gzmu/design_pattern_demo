package pers.lipeng.design_pattern.structural_mode._5_bridging_mode;

/**
 * 形状
 * 此处的颜色以构造函数传入，而不是通过继承的方式
 * 当然如果有多个属性我们可以通过set方法来为各个属性赋值
 */
abstract class Shape {
    protected Color color;
    public Shape(Color color){
        this.color = color;
    }
    public void show(){
        color.show();
    }
}
