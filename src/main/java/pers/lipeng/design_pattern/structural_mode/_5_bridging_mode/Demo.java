package pers.lipeng.design_pattern.structural_mode._5_bridging_mode;

class Demo {
    public static void main(String[] args) {
        //创建颜色
        Color red =new RedColor();
        Color green =new GreenColor();

        //创建红色正方形
        Shape circle =new SquareShape(red);
        circle.show();
        System.out.println("=================");

        //创建绿色正方形
        Shape square =new SquareShape(green);
        square.show();
        System.out.println("=================");

        //创建红色三角形
        Shape triangle =new TriangleShape(red);
        triangle.show();
        System.out.println("=================");

        //创建绿色三角形
        Shape triangle2 =new TriangleShape(green);
        triangle2.show();
        System.out.println("=================");
    }
}
