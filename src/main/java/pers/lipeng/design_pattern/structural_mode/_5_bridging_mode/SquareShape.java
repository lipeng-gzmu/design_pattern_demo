package pers.lipeng.design_pattern.structural_mode._5_bridging_mode;

/**
 * 正方形
 */
class SquareShape extends Shape {
    public SquareShape(Color color) {
        super(color);
    }

    @Override
    public void show() {
        System.out.println("正方形");
        color.show();
    }
}
