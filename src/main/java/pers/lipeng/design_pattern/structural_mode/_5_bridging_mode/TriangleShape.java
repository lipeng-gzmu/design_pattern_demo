package pers.lipeng.design_pattern.structural_mode._5_bridging_mode;

class TriangleShape extends Shape{
    public TriangleShape(Color color){
        super(color);
    }

    @Override
    public void show() {
        System.out.println("三角形");
        super.show();
    }
}
