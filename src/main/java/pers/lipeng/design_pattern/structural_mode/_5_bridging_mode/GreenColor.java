package pers.lipeng.design_pattern.structural_mode._5_bridging_mode;

class GreenColor extends Color {
    @Override
    void show() {
        System.out.println("绿色");
    }
}
