package pers.lipeng.design_pattern.structural_mode._5_bridging_mode;

abstract class Color {
    abstract void show();
}
