package pers.lipeng.design_pattern.structural_mode._5_bridging_mode;

class RedColor extends Color {
    @Override
    void show() {
        System.out.println("红色");
    }
}
