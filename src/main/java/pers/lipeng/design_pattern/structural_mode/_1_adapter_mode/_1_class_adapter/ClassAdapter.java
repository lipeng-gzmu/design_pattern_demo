package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._1_class_adapter;


/**
 * 适配器
 * Adapter
 * 继承AndroidInterface，实现TypeCInterface
 * 通过继承的方式实现适配器功能
 * 适配器就像生活中的转接线一样
 * 类适配器：适配器通过类来实现，以类来继承和实现接口的方式，来获取被适配类的信息并转换输出重写到适配接口。
 */
class ClassAdapter extends AndroidInterface implements TypeCInterface {
    @Override
    public void typeCRun() {
        super.androidRun();
    }
}
