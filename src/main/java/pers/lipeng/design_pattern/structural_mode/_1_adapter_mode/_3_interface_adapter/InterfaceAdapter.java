package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._3_interface_adapter;

/**
 * 接口适配器：当不需要全部实现接口提供的方法时，
 * 可先设计一个抽象类实现接口，
 * 并为该接口中每个方法提供一个默认实现（空方法），
 * 那么该抽象类的子类可有选择地覆盖父类的某些方法来实现需求。
 */
abstract class InterfaceAdapter extends AndroidInterface implements TypeCInterface{
    @Override
    public void typeCRun() {

    }

    @Override
    public void usbRun() {

    }

    @Override
    public void iphoneRun() {

    }
}
