package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._1_class_adapter;

/**
 * android接口
 * Adaptee
 */
class AndroidInterface {
    public void androidRun() {
        System.out.println("Android run");
    }
}
