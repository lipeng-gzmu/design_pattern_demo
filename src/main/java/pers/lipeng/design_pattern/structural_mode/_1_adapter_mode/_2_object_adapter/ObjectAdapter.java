package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._2_object_adapter;

/**
 * 适配器
 * Adapter
 * 对象适配器：通过实例对象（构造器传递）来实现适配器，而不是再用继承，其余基本同类适配器。
 */
class ObjectAdapter implements TypeCInterface{
    private AndroidInterface androidInterface;

    public ObjectAdapter(AndroidInterface androidInterface) {
        this.androidInterface = androidInterface;
    }

    @Override
    public void typeCRun() {
        androidInterface.androidRun();
    }
}
