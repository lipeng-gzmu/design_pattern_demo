package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._1_class_adapter;

/**
 * Type-C接口
 * Target
 */
interface TypeCInterface {
    void typeCRun();
}
