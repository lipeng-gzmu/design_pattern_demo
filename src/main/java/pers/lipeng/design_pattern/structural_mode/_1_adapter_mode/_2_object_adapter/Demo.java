package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._2_object_adapter;

/**
 * 对象适配器模式
 *
 */
class Demo {
    public static void main(String[] args) {
        ObjectAdapter objectAdapter = new ObjectAdapter(new AndroidInterface());
        runInterface(objectAdapter);
    }

    public static void runInterface(TypeCInterface typeCInterface){
        typeCInterface.typeCRun();
    }
}
