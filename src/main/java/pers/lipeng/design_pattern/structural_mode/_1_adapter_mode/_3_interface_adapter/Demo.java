package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._3_interface_adapter;

class Demo {
    public static void main(String[] args) {
        runInterface(new InterfaceAdapter() {
            @Override
            public void typeCRun() {
                new AndroidInterface().androidRun();
            }
        });
    }

    public static void runInterface(TypeCInterface typeCInterface){
        typeCInterface.typeCRun();
    }
}
