package pers.lipeng.design_pattern.structural_mode._1_adapter_mode._1_class_adapter;

/**
 * 目标角色为TypeC接口现在通过适配器可使用type-c来给android充电
 */
class Demo {
    public static void main(String[] args) {
       runInterface(new ClassAdapter());
    }

    public static void runInterface(TypeCInterface typeCInterface){
        typeCInterface.typeCRun();
    }
}
