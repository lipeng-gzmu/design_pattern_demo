package pers.lipeng.design_pattern.building_mode._2_factory_mode._2_factory_method_mode;

/**
 * @author APengG
 */
class BaoMaCar implements Car {
    @Override
    public void blow() {
        System.out.println("我是BaoMaCar, 啊噗噗噗~ 啊噗噗噗~");
    }
}
