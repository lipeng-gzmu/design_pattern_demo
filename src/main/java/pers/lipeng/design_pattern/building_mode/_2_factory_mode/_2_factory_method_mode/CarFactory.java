package pers.lipeng.design_pattern.building_mode._2_factory_mode._2_factory_method_mode;

interface CarFactory {
    /**
     * 创建轿车
     * @return Car
     */
    Car createCar();
}
