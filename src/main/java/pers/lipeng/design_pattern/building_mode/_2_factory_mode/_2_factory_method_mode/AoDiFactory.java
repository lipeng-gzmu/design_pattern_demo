package pers.lipeng.design_pattern.building_mode._2_factory_mode._2_factory_method_mode;

class AoDiFactory implements CarFactory {
    @Override
    public Car createCar() {
        return new AoDiCar();
    }
}
