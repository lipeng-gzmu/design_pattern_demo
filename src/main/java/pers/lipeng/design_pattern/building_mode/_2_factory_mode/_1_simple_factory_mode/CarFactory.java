package pers.lipeng.design_pattern.building_mode._2_factory_mode._1_simple_factory_mode;

/**
 * @author APengG
 * 简单工厂核心类
 * 通过传入参数决定创建什么对象
 * 可以是名称也可以是其他
 */
class CarFactory {
    public static Car createCar(String name){
        if(name == null || "".equals(name)){
            return null;
        }
        if("aodi".equals(name)){
            return new AoDiCar();
        }
        if("baoma".equals(name)){
            return new BaoMaCar();
        }
        return null;
    }
}
