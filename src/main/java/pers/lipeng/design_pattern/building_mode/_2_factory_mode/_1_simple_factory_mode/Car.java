package pers.lipeng.design_pattern.building_mode._2_factory_mode._1_simple_factory_mode;

/**
 * @author APengG
 * 将汽车抽象出来
 * 为其定义一个方法run()
 * 任何汽车类型实现Car
 */
interface Car {
    /**
     * 汽车的鸣笛方法
     */
    void blow();
}
