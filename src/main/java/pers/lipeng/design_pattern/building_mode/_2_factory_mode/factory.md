# 什么是工厂模式

+ 它提供了一种创建对象的最佳方式。在工厂模式中，我们在创建对象时不会对客户端暴露创建逻 辑，并且是通过使用一个共同的接口来指向新创建的对象。实现了创建者和调用者分离，工厂模式 分为简单工厂、工厂方法、抽象工厂模式

# 工厂模式好处

+ 工厂模式是我们最常用的实例化对象模式了，是用工厂方法代替new操作的一种模式。
+  利用工厂模式可以降低程序的耦合性，为后期的维护修改提供了很大的便利。 
+ 将选择实现类、创建对象统一管理和控制。从而将调用者跟我们的实现类解耦。

# 为什么学习工厂

+ 不知道你们面试题问到过源码没有，你知道Spring的源码吗，MyBatis的源码吗，等等等 如果你想 学习很多框架的源码，或者你想自己开发自己的框架，就必须先掌握设计模式（工厂设计模式用的 是非常非常广泛的）

# spring涉及的工厂

1. Spring IOC
   + 看过Spring源码就知道，在Spring IOC容器创建bean的过程是使用了工厂设计模式 
   + Spring中无论是通过xml配置还是通过配置类还是注解进行创建bean，大部分都是通过简单工厂 来进行创建的。 
   + 当容器拿到了beanName和class类型后，动态的通过反射创建具体的某个对象，最后将创建的对 象放到Map中。

2. 为什么spring ioc使用工厂模式创建bean
   + 在实际开发中，如果我们A对象调用B，B调用C，C调用D的话我们程序的耦合性就会变高。（耦合 大致分为类与类之间的依赖，方法与方法之间的依赖。） 
   + 在很久以前的三层架构编程时，都是控制层调用业务层，业务层调用数据访问层时，都是是直接 new对象，耦合性大大提升，代码重复量很高，对象满天飞 ，为了避免这种情况，Spring使用工厂模式编程，写一个工厂，由工厂创建Bean，以后我们如果要 对象就直接管工厂要就可以，剩下的事情不归我们管了。Spring IOC容器的工厂中有个静态的 Map集合，是为了让工厂符合单例设计模式，即每个对象只生产一次，生产出对象后就存入到 Map集合中，保证了实例不会重复影响程序效率。



# 工厂模式分类

+ 简单工厂

  用来生产同一等级结构中的任意产品。（不支持拓展增加产品）

+ 工厂方法

  用来生产同一等级结构中的固定产品。（支持拓展增加产品）

+ 抽象工厂

  用来生产不同产品族的全部产品。（不支持拓展增加产品；支持增加产品族）

