package pers.lipeng.design_pattern.building_mode._2_factory_mode._3_abstract_factory_mode;

interface Car {
    /**
     * 鸣笛
     */
    void blow();
}
