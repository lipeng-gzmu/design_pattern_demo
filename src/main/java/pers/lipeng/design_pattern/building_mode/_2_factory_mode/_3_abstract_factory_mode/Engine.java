package pers.lipeng.design_pattern.building_mode._2_factory_mode._3_abstract_factory_mode;

/**
 *  汽车的引擎
 */
interface Engine {
    /**
     * 执行
     */
    void run();
}
