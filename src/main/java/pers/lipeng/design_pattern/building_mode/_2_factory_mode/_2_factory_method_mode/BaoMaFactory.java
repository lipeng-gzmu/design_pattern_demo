package pers.lipeng.design_pattern.building_mode._2_factory_mode._2_factory_method_mode;
 class BaoMaFactory implements CarFactory{
    @Override
    public Car createCar() {
        return new BaoMaCar();
    }
}
