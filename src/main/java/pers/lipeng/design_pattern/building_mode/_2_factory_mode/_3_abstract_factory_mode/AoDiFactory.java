package pers.lipeng.design_pattern.building_mode._2_factory_mode._3_abstract_factory_mode;

/**
 * AoDi产品族
 */
class AoDiFactory implements TotalFactory{
    @Override
    public Car createCar() {
        return new AoDiCar();
    }

    @Override
    public Engine createEngine() {
        return new AoDiEngine();
    }
}
