package pers.lipeng.design_pattern.building_mode._2_factory_mode._3_abstract_factory_mode;

/**
 为访问类提供一个创建一组相关或相互依赖对象的接口，
 且访问类无须指定所要产品的具体类就能得到同族的不同等级的产品的模式结构。
 抽象工厂模式是工厂方法模式的升级版本，工厂方法模式只生产一个等级的产品，
 而抽象工厂模式可生产多个等级的产品。

 可理解为比工厂方法又多了一维
 简单工厂, 是针对一种"类型"的抽象;
 工厂方法, 是针对一种"类型", 以及一种"创建方法"的抽象;
 抽象工厂, 是针对一组"类型"与"创建方法"的抽象, 组内每一套类型与创建方法一一对应.
 */
class Demo {
    public static void main(String[] args) {
        //生产AoDi的汽车与发动机
        Car car = new AoDiFactory().createCar();
        Engine engine = new AoDiFactory().createEngine();
        car.blow();
        engine.run();


        //自定义改装工厂，我偏要改装汽车让AoDi用BaoMa的发动机
        System.out.println("==========================");
        Car car1 = new CustomFactory().createCar();
        Engine engine1 = new CustomFactory().createEngine();
        car1.blow();
        engine1.run();

    }
}
