package pers.lipeng.design_pattern.building_mode._2_factory_mode._2_factory_method_mode;

interface Car {
    /**
     * 鸣笛
     */
    void blow();
}
