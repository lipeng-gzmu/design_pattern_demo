package pers.lipeng.design_pattern.building_mode._2_factory_mode._3_abstract_factory_mode;

/**
 * 抽象工厂指生产一个产品族
 * 如下为汽车与汽车使用的引擎。
 * 我们还可以写一些其他的产品
 * 如轮胎，座椅等
 */
interface TotalFactory {
    /**
     * 生产汽车
     * @return Car
     */
    Car createCar();

    /**
     * 引擎
     * @return Engine
     */
    Engine createEngine();
}
