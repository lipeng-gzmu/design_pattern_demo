package pers.lipeng.design_pattern.building_mode._2_factory_mode._2_factory_method_mode;

/**
 * @author APengG
 */
class AoDiCar implements Car {
    @Override
    public void blow() {
        System.out.println("我是AoDiCar,嗷呜~ 嗷呜~ 嗷呜~");
    }
}
