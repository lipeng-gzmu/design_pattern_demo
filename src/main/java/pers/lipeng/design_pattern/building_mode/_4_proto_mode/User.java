package pers.lipeng.design_pattern.building_mode._4_proto_mode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author APengG
 创建对象实现Cloneable接口
 */
class User implements Cloneable {
  private int age;
  private String name;
  private String sex;
  private String high;
  private ArrayList<String> phones;

  private List<User> girlFriends;

  @Override
  protected User clone(){
     try{
        User user = (User) super.clone();

        //引用类型深拷贝,如不需设置深拷贝则不需要添加该行
        this.phones = (ArrayList<String>)this.phones.clone();
        return user;
     }catch (CloneNotSupportedException e){
       e.printStackTrace();
     }
     return null;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getHigh() {
    return high;
  }

  public void setHigh(String high) {
    this.high = high;
  }

  public List<String> getPhones() {
    return phones;
  }

  public void setPhones(ArrayList<String> phones) {
    this.phones = phones;
  }

  public List<User> getGirlFriends() {
    return girlFriends;
  }

  public void setGirlFriends(List<User> girlFriends) {
    this.girlFriends = girlFriends;
  }
}
