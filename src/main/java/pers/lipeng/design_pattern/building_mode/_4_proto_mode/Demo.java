package pers.lipeng.design_pattern.building_mode._4_proto_mode;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 原型实现
 */
class Demo {
    public static void main(String[] args) {
        //定义一个原型
        User protoUser = new User();
        protoUser.setName("李四");
        protoUser.setAge(30);
        protoUser.setHigh("183cm");
        protoUser.setSex("男");
        protoUser.setPhones(new ArrayList<>(Arrays.asList("1111","22222","33333")));


        //执行拷贝方法
        User clone = protoUser.clone();
        clone.setSex("女");
        clone.setPhones(new ArrayList<>(Arrays.asList("ddd")));
        System.out.println("两个对象是否为同一个：\r\n"+(protoUser == clone));
        System.out.println("查看属性内容:\r\n"+
                protoUser.getName()+"--->"+clone.getName()+" 是否为同一对象:"+(protoUser.getName()==clone.getName())+"\r\n"+
                protoUser.getAge()+"--->"+clone.getAge()+"\r\n"+
                protoUser.getHigh()+"--->"+clone.getHigh()+" 是否为同一对象:"+(protoUser.getAge()==clone.getAge())+"\r\n"+
                protoUser.getSex()+"--->"+clone.getSex()+" 是否为同一对象:"+(protoUser.getSex()==clone.getSex())+"\r\n"
        );
        System.out.println("protoUser.getPhone()");
        protoUser.getPhones().forEach(System.out::println);
        System.out.println("clone.getPhones()");
        clone.getPhones().forEach(System.out::println);
        System.out.println("引用类拷贝是否为同一个对象：\r\n"+(protoUser.getPhones()==clone.getPhones()));
    }
}
