package pers.lipeng.design_pattern.building_mode._3_builder_mode;

/**
 * 构造类，对各种参数设置统一一种标准
 */
interface CarBuilder {
    void buildName(String name);
    void buildProduceTime(String produceTime);
    void buildEnginTag(String engineTag);
    void buildColor(String color);
    void buildVersion(String version);
    Car build();
}
