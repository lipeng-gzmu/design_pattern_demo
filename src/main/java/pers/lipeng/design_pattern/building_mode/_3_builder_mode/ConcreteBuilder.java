package pers.lipeng.design_pattern.building_mode._3_builder_mode;

/**
 * 建造的具体实现
 */
class ConcreteBuilder implements CarBuilder{
    Car car = new Car();
    @Override
    public void buildName(String name) {
        car.setName(name);
    }

    @Override
    public void buildProduceTime(String produceTime) {
        car.setProduceTime(produceTime);
    }

    @Override
    public void buildEnginTag(String engineTag) {
        car.setEngineTag(engineTag);
    }

    @Override
    public void buildColor(String color) {
        car.setColor(color);
    }

    @Override
    public void buildVersion(String version) {
        car.setVersion(version);
    }

    @Override
    public Car build() {
        return car;
    }
}
