package pers.lipeng.design_pattern.building_mode._3_builder_mode;

class Car {
 private String name;
 private String produceTime;
 private String engineTag;
 private String color;
 private String version;

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", produceTime='" + produceTime + '\'' +
                ", engineTag='" + engineTag + '\'' +
                ", color='" + color + '\'' +
                ", version='" + version + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(String produceTime) {
        this.produceTime = produceTime;
    }

    public String getEngineTag() {
        return engineTag;
    }

    public void setEngineTag(String engineTag) {
        this.engineTag = engineTag;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
