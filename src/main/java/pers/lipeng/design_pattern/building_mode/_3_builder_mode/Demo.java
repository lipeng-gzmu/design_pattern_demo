package pers.lipeng.design_pattern.building_mode._3_builder_mode;

class Demo {
    public static void main(String[] args) {
        CarDirector carDirector = new CarDirector();

        Car aoDi = carDirector.buildAuDiCar(new ConcreteBuilder());
        System.out.println(aoDi);

        Car baoMa = carDirector.buildBaoMa(new ConcreteBuilder());
        System.out.println(baoMa);
    }
}
