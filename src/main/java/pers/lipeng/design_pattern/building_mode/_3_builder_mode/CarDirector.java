package pers.lipeng.design_pattern.building_mode._3_builder_mode;

/**
 * @author APengG
 */
class CarDirector {

    public Car buildAuDiCar(CarBuilder carBuilder){
        carBuilder.buildName("Aodi");
        carBuilder.buildColor("绿色");
        carBuilder.buildEnginTag("三刚");
        carBuilder.buildProduceTime("现在");

        return carBuilder.build();
    }


    public Car buildBaoMa(CarBuilder carBuilder){
        carBuilder.buildName("BaoMa");
        carBuilder.buildColor("红色");
        carBuilder.buildEnginTag("三刚");
        carBuilder.buildProduceTime("现在");

        return carBuilder.build();
    }
}
