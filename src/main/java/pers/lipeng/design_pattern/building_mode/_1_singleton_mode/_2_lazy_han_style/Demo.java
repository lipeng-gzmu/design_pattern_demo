package pers.lipeng.design_pattern.building_mode._1_singleton_mode._2_lazy_han_style;

/**
 * @author APengG
 * 懒汉式单例模式
 * 优点：延时加载，线程天生安全，调用效率不高
 * 缺点：不能延时加载
 * 类初始化时，不会初始化该对象，真正需要使用的时候才会创建该对象，具备延时加载的效果，但是线程天生安全，调用效率不高。
 */
class Demo {
    private  static Demo demo;
    private static boolean flag = false;
    public Demo(){
        if (!flag){
            flag = true;
        }else{
            throw new RuntimeException("单例模式被侵犯！");
        }
    }

    public static Demo getInstance(){
        if (demo == null){
            demo = new Demo();
        }
        return demo;
    }


    public static void main(String[] args) {
        Demo demo1 = Demo.getInstance();
        Demo demo2 = Demo.getInstance();
        System.out.println(demo1 == demo2);
    }
}
