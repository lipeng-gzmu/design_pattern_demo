package pers.lipeng.design_pattern.building_mode._1_singleton_mode._4_enum_style;

/**
 * @author APengG
 * 枚举式单例模式
 * 优点：延时加载，线程天生安全，调用效率高
 * 枚举类没有构造方法，所以不能被反射破坏
 */
@SuppressWarnings("unused")
class Demo {

    public static Demo getInstance(){
        return DemoInstance.INSTANCE.getInstance();
    }

    /**
     * 创建枚举类
     */
    private  enum DemoInstance{
        /**
         * INSTANCE
         */
        INSTANCE;

        private final Demo demo;
        DemoInstance(){
            demo = new Demo();
        }

        public Demo getInstance(){
            return demo;
        }
    }
}
