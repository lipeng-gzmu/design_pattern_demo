package pers.lipeng.design_pattern.building_mode._1_singleton_mode._1_hungry_han_style;

/**
 * @author APengG
 * 饿汉式单例模式
 * 优点：线程安全，调用效率高
 * 缺点：不能延时加载
 * 类初始化时,会立即加载该对象，线程天生安全,调用效率高。
 */
class Demo {

    private final  static Demo DEMO = new Demo();
    private static boolean flag = false;

    public Demo() {
        if(!flag) {
            flag = true;
        } else {
            throw new RuntimeException("单例模式被侵犯！");
        }
    }

    public static Demo getInstance() {
        return DEMO;
    }


    public static void main(String[] args) {
        Demo demo1 = Demo.getInstance();
        Demo demo2 = Demo.getInstance();
        System.out.println(demo1 == demo2);
    }
}
