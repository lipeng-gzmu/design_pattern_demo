package pers.lipeng.design_pattern.building_mode._1_singleton_mode._3_static_class_style;

/**
 * @author APengG
 * 静态内部类式单例模式
 * 优点：延时加载，线程天生安全，调用效率高
 */
class Demo {
    private static Demo demo;
    private static boolean flag = false;

    public Demo(){
        if (!flag){
            flag = true;
        }else{
            throw new RuntimeException("单例模式被侵犯！");
        }
    }

    public static class DemoInstance{
        private static final Demo DEMO = new Demo();
    }

    public static Demo getInstance(){
        return DemoInstance.DEMO;
    }

    public static void main(String[] args) {
        Demo demo1 = Demo.getInstance();
        Demo demo2 = Demo.getInstance();
        System.out.println(demo1 == demo2);
    }
}
