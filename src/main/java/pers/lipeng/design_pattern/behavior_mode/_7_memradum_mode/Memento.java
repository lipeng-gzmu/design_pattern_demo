package pers.lipeng.design_pattern.behavior_mode._7_memradum_mode;

/**
 * 备忘录对象
 */
class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
