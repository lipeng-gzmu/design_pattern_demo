package pers.lipeng.design_pattern.behavior_mode._7_memradum_mode;

import java.util.ArrayList;
import java.util.List;

/**
 * 备忘录管理者对象
 */
class CareTaker {
    private List<Memento> mementos = new ArrayList<>();

    public void addMemento(Memento memento){
        mementos.add(memento);
    }


    public Memento getMemento(int index){
        return mementos.get(index);
    }
}
