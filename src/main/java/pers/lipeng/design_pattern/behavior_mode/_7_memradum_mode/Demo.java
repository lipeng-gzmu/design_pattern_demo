package pers.lipeng.design_pattern.behavior_mode._7_memradum_mode;

class Demo {
    public static void main(String[] args) {
        Originator originator = new Originator();
        //备忘录管理者
        CareTaker careTaker = new CareTaker();

        originator.setState("state 1");
        careTaker.addMemento(originator.saveMemento());

        originator.setState("state 2");
        careTaker.addMemento(originator.saveMemento());

        originator.setState("state 3");
        careTaker.addMemento(originator.saveMemento());

        originator.setState("state 4");
        careTaker.addMemento(originator.saveMemento());

        System.out.println("当前状态：" + originator.getState());
        //恢复到状态1
        originator.getStateFromMemento(careTaker.getMemento(0));
        System.out.println("恢复到状态1：" + originator.getState());
        //恢复到状态2
        originator.getStateFromMemento(careTaker.getMemento(1));
        System.out.println("恢复到状态2：" + originator.getState());
        //恢复到状态3
        originator.getStateFromMemento(careTaker.getMemento(2));
        System.out.println("恢复到状态3：" + originator.getState());
        //恢复到状态4
        originator.getStateFromMemento(careTaker.getMemento(3));
        System.out.println("恢复到状态4：" + originator.getState());
    }
}
