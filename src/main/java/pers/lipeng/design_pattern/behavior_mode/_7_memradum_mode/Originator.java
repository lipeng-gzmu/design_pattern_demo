package pers.lipeng.design_pattern.behavior_mode._7_memradum_mode;

/**
 * 记录者对象
 */
class Originator {
    private String state;


    public String getState(){
        return state;
    }
    public void setState(String state){
        this.state = state;
    }

    public Memento saveMemento(){
        return new Memento(state);
    }


    public void getStateFromMemento(Memento memento){
        state = memento.getState();
    }


}
