package pers.lipeng.design_pattern.behavior_mode._4_iterative_sub_mode;

/**
 * 具体迭代角色
 * 类似于java的Iterator接口的实现类
 */
class ConcreateCustomIterator implements CustomIterator{
    private ConcreteAggregate aggregate;
    private int index = 0;
    private int size = 0;

    public ConcreateCustomIterator(ConcreteAggregate aggregate){
        this.aggregate = aggregate;
        this.size = aggregate.size();
        index = 0;
    }
    @Override
    public void first() {
        index = 0;
    }

    @Override
    public void next() {
        if(index < size){
            index++;
        }
    }

    @Override
    public boolean hasNext() {
        return index < size;
    }

    @Override
    public Object currentItem() {
        return aggregate.getElement(index);
    }
}
