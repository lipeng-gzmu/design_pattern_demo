package pers.lipeng.design_pattern.behavior_mode._4_iterative_sub_mode;

/**
 * 抽象迭代角色
 * 类似于java的Iterator接口
 */
interface CustomIterator {
    void first();

    void next();

    boolean hasNext();

    Object currentItem();
}
