package pers.lipeng.design_pattern.behavior_mode._4_iterative_sub_mode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Demo {
    public static void main(String[] args) {
        Object[] array  = new Object[]{"a","b","c","d","e"};
        Aggregate aggregate = new ConcreteAggregate(array);

        CustomIterator iterator = aggregate.createIterator();
        while (iterator.hasNext()){
            System.out.println(iterator.currentItem());
            iterator.next();
        }
        List<Integer> list = new ArrayList<>();
        Iterator<Integer> iterator1 = list.iterator();

    }
}
