package pers.lipeng.design_pattern.behavior_mode._4_iterative_sub_mode;

/**
 * 具体聚合角色
 * 类似于java的Collection接口的实现类,如ArrayList,LinkedList等
 */
class ConcreteAggregate extends Aggregate{
    private Object[] objArray = null;

    public ConcreteAggregate(Object[] objArray){
        this.objArray = objArray;
    }

    public Object getElement(int i){
        if(i < objArray.length){
            return objArray[i];
        }else{
            return null;
        }
    }

    public int size(){
        return objArray.length;
    }
    @Override
    CustomIterator createIterator() {
        return new ConcreateCustomIterator(this);
    }
}
