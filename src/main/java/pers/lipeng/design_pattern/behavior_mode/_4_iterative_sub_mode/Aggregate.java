package pers.lipeng.design_pattern.behavior_mode._4_iterative_sub_mode;

/**
 * 抽象聚合角色
 * 类似于java的List接口
 */
abstract class Aggregate {
    /**
     * 工厂方法，创建相应迭代子对象的接口
     * @return
     */
    abstract CustomIterator createIterator();
}
