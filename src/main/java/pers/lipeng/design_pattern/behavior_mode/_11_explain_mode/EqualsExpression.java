package pers.lipeng.design_pattern.behavior_mode._11_explain_mode;

class EqualsExpression implements Expression{
    private String data;
    public EqualsExpression(String data){
        this.data = data;
    }

    @Override
    public boolean interpret(String context) {
        return data.equals(context);
    }
}
