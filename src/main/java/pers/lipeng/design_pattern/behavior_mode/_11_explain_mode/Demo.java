package pers.lipeng.design_pattern.behavior_mode._11_explain_mode;

class Demo {
    public static Expression getExpression1(){
        //包含hello,或则包含world,或则等于hello world
        Expression expression1 = new ContainExpression("hello");
        Expression expression2 = new ContainExpression("world");
        Expression expression3 = new EqualsExpression("hello world");

        Expression expression4 = new OrExpression(expression1, expression2);
        return new OrExpression(expression4, expression3);
    }

    public static Expression getExpression2(){
        //同时包含hello和world
        Expression expression1 = new ContainExpression("hello");
        Expression expression2 = new ContainExpression("world");
        return new AndExpression(expression1, expression2);
    }
    public static void main(String[] args) {
        Expression expression1 = getExpression1();
        System.out.println(expression1.interpret("hello"));
        System.out.println(expression1.interpret("world"));
        System.out.println(expression1.interpret("hello world"));
        System.out.println(expression1.interpret("hell word 1"));


        System.out.println("=====================================");
        Expression expression2 = getExpression2();
        System.out.println(expression2.interpret("hello"));
        System.out.println(expression2.interpret("world"));
        System.out.println(expression2.interpret("hello world"));
    }
}
