package pers.lipeng.design_pattern.behavior_mode._11_explain_mode;

class ContainExpression implements Expression{
    private String data;
    public ContainExpression(String data){
        this.data = data;
    }
    @Override
    public boolean interpret(String context) {
        if (context == null || "".equals(context)){
            return false;
        }
        return context.contains(data);
    }
}
