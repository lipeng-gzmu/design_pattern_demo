package pers.lipeng.design_pattern.behavior_mode._11_explain_mode;

interface Expression {
    boolean interpret(String context);
}
