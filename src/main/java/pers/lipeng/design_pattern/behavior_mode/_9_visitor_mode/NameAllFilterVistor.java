package pers.lipeng.design_pattern.behavior_mode._9_visitor_mode;

/**
 * 具体访问者
 * 展示全部元素
 */
class NameAllFilterVistor implements Visitor{
    @Override
    public void visit(Direction direction) {
        System.out.println("展示目录：" + direction.name);
    }

    @Override
    public void visit(File file) {
        System.out.println("展示文件：" + file.name);
    }
}
