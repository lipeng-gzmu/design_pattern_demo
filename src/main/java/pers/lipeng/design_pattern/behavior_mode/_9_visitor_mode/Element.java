package pers.lipeng.design_pattern.behavior_mode._9_visitor_mode;

/**
 * 元素角色
 */
abstract class Element {
    protected String name;
    protected long size;

    public Element(String name,long size) {
        super();
        this.name = name;
        this.size = size;
    }

    abstract void accept(Visitor visitor);

}
