package pers.lipeng.design_pattern.behavior_mode._9_visitor_mode;

/**
 * 具体元素
 */
class File extends Element{

    public File(String name, long size) {
        super(name,size);
    }

    @Override
    void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
