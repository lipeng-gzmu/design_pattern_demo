package pers.lipeng.design_pattern.behavior_mode._9_visitor_mode;

class NamePrefixFilterVistor implements Visitor {
    private String prefix;
    public NamePrefixFilterVistor(String prefix) {
        this.prefix = prefix;
    }
    @Override
    public void visit(Direction direction) {
        if (direction.name.startsWith(prefix)){
            System.out.println("展示目录：" + direction.name);
          }
    }

    @Override
    public void visit(File file) {
        if (file.name.startsWith(prefix)){
            System.out.println("展示文件：" + file.name);
          }
    }
}
