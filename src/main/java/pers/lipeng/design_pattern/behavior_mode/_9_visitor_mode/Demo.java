package pers.lipeng.design_pattern.behavior_mode._9_visitor_mode;

import java.util.ArrayList;
import java.util.List;

/**
 * 目前为全部访问与文件目录前缀名访问
 * 我们可以扩展一个文件大小访问的过滤器
 */
class Demo {
    public static void main(String[] args) {
        List<Element> elements = new ArrayList<>();
        elements.add(new Direction("java direction", 1000));
        elements.add(new Direction("python direction", 1000));
        elements.add(new File("java ee.txt", 100));
        elements.add(new File("java se.txt", 100));
        elements.add(new File("python 2.txt", 100));
        elements.add(new File("python 3.txt", 100));
        Visitor nameAllFilterVistor = new NameAllFilterVistor();
        for (Element element : elements) {
            element.accept(nameAllFilterVistor);
        }

        System.out.println("====================================");
        Visitor namePrefixFilterVisitor = new NamePrefixFilterVistor("java");
        for (Element element : elements) {
            element.accept(namePrefixFilterVisitor);
        }
    }
}
