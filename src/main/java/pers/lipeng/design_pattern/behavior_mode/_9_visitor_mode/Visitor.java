package pers.lipeng.design_pattern.behavior_mode._9_visitor_mode;

/**
 * 抽象访问者
 */
interface Visitor {
    void visit(Direction direction);
    void visit(File file);
}
