package pers.lipeng.design_pattern.behavior_mode._6_command_mode;

/**
 * 具体命令类
 * 关灯命令
 */
class LightOffCommand extends Command{
    private Light light;

    public LightOffCommand() {
        this.light = new Light();
    }
    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void excute() {
        light.off();
    }

    @Override
    public void undo() {
        light.on();
    }
}
