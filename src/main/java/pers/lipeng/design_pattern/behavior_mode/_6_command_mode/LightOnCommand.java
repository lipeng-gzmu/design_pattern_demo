package pers.lipeng.design_pattern.behavior_mode._6_command_mode;

/**
 * 具体命令类
 * 开灯命令
 */
class LightOnCommand  extends Command{
    private Light light;

    public LightOnCommand() {
        this.light = new Light();
    }
    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void excute() {
        light.on();
    }

    @Override
    public void undo() {
        light.off();
    }
}
