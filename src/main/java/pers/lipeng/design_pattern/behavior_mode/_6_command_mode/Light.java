package pers.lipeng.design_pattern.behavior_mode._6_command_mode;

/**
 * 命令接收者-灯
 */
class Light {
    private String status;
    public Light(){
        status = "灯已关闭";
    }
    public void on() {
        status = "灯已打开";
        System.out.println("当前灯的状态：" + status);
    }

    public void off() {
        status = "灯已关闭";
        System.out.println("当前灯的状态：" + status);
    }
}
