package pers.lipeng.design_pattern.behavior_mode._6_command_mode;

/**
 * 抽象命令类
 */
abstract class Command {
   /**
    * 执行命令
    */
   public abstract void excute();

   /**
    * 撤销命令
    */
   public abstract void undo();
}
