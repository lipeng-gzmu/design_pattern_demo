package pers.lipeng.design_pattern.behavior_mode._6_command_mode;

class Demo {
    public static void main(String[] args) {
        Light light  = new Light();
        Command onCommand = new LightOnCommand(light);
        Command offCommand = new LightOffCommand(light);

        // 创建遥控器
        Invoker invoker = new Invoker();

        System.out.println("发起开灯命令");
        invoker.sendCommand(onCommand);
        invoker.action();

        System.out.println("撤销操作");
        invoker.undo();

        System.out.println("发起关灯命令");
        invoker.sendCommand(offCommand);
        invoker.action();

        System.out.println("撤销操作");
        invoker.undo();
    }
}
