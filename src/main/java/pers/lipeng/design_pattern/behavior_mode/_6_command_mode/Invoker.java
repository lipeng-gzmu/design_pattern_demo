package pers.lipeng.design_pattern.behavior_mode._6_command_mode;
/**
 * 命令调用者
 */
class Invoker {
    private Command command;

    public void sendCommand(Command command) {
        this.command = command;
    }

    /**
     * 执行命令
     */
    public void action() {
        command.excute();
    }

    /**
     * 撤销操作
     */
    public void undo() {
        command.undo();
    }
}
