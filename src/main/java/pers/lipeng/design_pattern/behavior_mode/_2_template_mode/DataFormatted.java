package pers.lipeng.design_pattern.behavior_mode._2_template_mode;

/**
 * 抽象模板类
 * 对于数据的一些验证操作为公共操作
 * 因此抽象到具体方法中
 * 对于数据的处理操作为不同的操作
 * 因此抽象到抽象方法中
 */
abstract class DataFormatted {
    public void dataFormat(String data) {
        System.out.println("验证数据格式");
        System.out.println("验证数据完整性");
        System.out.println("验证数据正确性");
        System.out.println("验证数据安全性");
        System.out.println("验证数据是否被篡改");
        System.out.println("验证数据是否被破坏");
        System.out.println("验证数据是否被修改");
        String s = dealData(data);
        System.out.println("数据处理结果：" + s);
    }

    abstract String dealData(String data);
}
