package pers.lipeng.design_pattern.behavior_mode._2_template_mode;

class MapDataFormatted extends DataFormatted{
    @Override
    String dealData(String data) {
        return "数据  "+data+"  已被格式化为Map格式";
    }
    }
