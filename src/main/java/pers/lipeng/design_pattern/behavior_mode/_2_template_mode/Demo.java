package pers.lipeng.design_pattern.behavior_mode._2_template_mode;

class Demo {
    public static void main(String[] args) {
        String data = "haha";

        JsonDataFormatted jsonDataFormatted = new JsonDataFormatted();
        jsonDataFormatted.dataFormat(data);

        MapDataFormatted mapDataFormatted = new MapDataFormatted();
        mapDataFormatted.dataFormat(data);
    }
}
