package pers.lipeng.design_pattern.behavior_mode._2_template_mode;

class JsonDataFormatted extends DataFormatted{
    @Override
    String dealData(String data) {
        return "数据  "+data+"  已被格式化为Json格式";
    }
}
