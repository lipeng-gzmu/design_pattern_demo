package pers.lipeng.design_pattern.behavior_mode._10_mediator_mode;

class Demo {
    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom();
        User user1 = new User("Robert");
        User user2 = new User("John");

        user1.sendMessage(chatRoom,"Hi! everyBody!");
        user2.sendMessage(chatRoom,"Hello! Robert!");
        user1.sendMessage(chatRoom,"Nice to meet you!");
    }
}
