package pers.lipeng.design_pattern.behavior_mode._10_mediator_mode;

class User {
    private String name;

    public User(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
    public void sendMessage(ChatRoom chatRoom,String message){
        chatRoom.showMessage(this,message);
    }

    @Override
    public String toString(){
        return "User [name=" + name + "]";
    }
}
