package pers.lipeng.design_pattern.behavior_mode._10_mediator_mode;

import java.util.Date;

/**
 * 中介者模式
 */
class ChatRoom {
    public void showMessage(User user,String message){
        System.out.println(new Date().toString()+" ["+user.getName()+"]:"+message);
    }
}
