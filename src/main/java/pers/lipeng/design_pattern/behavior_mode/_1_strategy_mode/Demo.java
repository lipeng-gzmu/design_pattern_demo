package pers.lipeng.design_pattern.behavior_mode._1_strategy_mode;

/**
 * 可以改善if-else
 * 当策略改变时是需要
 * 向上下文中传入新的策略即可
 * 无需修改原有代码
 */
class Demo {
    public static void main(String[] args) {
        PayStrategy winxinPay = new WexinPay();
        PayStrategy zhifuBaoPay = new ZhifuBaoPay();
        PayStrategy yinLianPay = new YinLianPay();
        Context context = new Context(winxinPay);
        context.pay();

        Context context1 = new Context(zhifuBaoPay);
        context1.pay();

        Context context2 = new Context(yinLianPay);
        context2.pay();
    }
}
