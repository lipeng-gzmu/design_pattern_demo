package pers.lipeng.design_pattern.behavior_mode._1_strategy_mode;

/**
 * 使用上下文维护支付策略
 * 便于使用该上下文切换支付策略
 */
class Context {
    private PayStrategy payStrategy;

    public Context(PayStrategy payStrategy) {
        this.payStrategy = payStrategy;
    }

    public void pay(){
        payStrategy.pay();
    }
}
