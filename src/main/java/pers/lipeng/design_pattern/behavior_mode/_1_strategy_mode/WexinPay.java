package pers.lipeng.design_pattern.behavior_mode._1_strategy_mode;

class WexinPay extends PayStrategy {
    @Override
    void pay() {
        System.out.println("微信支付");
    }
}
