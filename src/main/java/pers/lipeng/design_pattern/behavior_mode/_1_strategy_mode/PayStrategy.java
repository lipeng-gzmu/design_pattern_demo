package pers.lipeng.design_pattern.behavior_mode._1_strategy_mode;

/**
 * 支付策略
 */
abstract class PayStrategy {
    abstract void pay();
}
