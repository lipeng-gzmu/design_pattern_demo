package pers.lipeng.design_pattern.behavior_mode._1_strategy_mode;

class YinLianPay extends PayStrategy{
    @Override
    void pay() {
        System.out.println("银联支付");
    }
}
