package pers.lipeng.design_pattern.behavior_mode._5_chain_of_responsibility_mode;

class DebugLogger extends AbstractLogger{
    public DebugLogger(int level){
        this.level = level;
    }
    @Override
    void write(String message) {
        System.out.println("DebugLogger:"+message);
    }
}
