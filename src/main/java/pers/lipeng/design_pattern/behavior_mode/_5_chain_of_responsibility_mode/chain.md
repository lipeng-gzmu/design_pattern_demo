# 责任链模式

为请求创建了一个接收者对象的链。这种模式给予请求的类型，对请求的发送者和接收者进行解耦。这种类型的设计模式属于行为型模式。

在这种模式中，通常每个接收者都包含对另一个接收者的引用。如果一个对象不能处理该请求，那么它会把相同的请求传给下一个接收者，依此类推。



职责链上的处理者负责处理请求，客户只需要将请求发送到职责链上即可，无须关心请求的处理细节和请求的传递，所以职责链将请求的发送者和请求的处理者解耦了。



角色：

+ 处理者

  声明了所有具体处理者的通用接口，该接口通常仅包含单个方法用于请求处理，但有时其还会包含一个设置链上下处理者的方法。

+ 基础处理者

  基础处理者 是一个可选的类，你可以将所有处理者共用的样本代码放置在其中。(通常情况下，该类定义了一个保存对于下个处理者引用的成员变量。客户端可通过将处理者的构造函数或设定方法来创建链。该类还可以实现默认的处理行为，确定下个处理者存在后再将请求传递给它。

+ 具体处理者

  包含处理请求的实际代码。每个处理者接收到请求后，都必须决定是否进行处理，或者说是否沿着链传递请求。

+ 客户端

  可根据程序逻辑一次性或者动态的生成链。



优点：

1、降低耦合度。它将请求的发送者和接收者解耦。

 2、简化了对象。使得对象不需要知道链的结构。 

3、增强给对象指派职责的灵活性。通过改变链内的成员或者调动它们的次序，允许动态地新增或者删除责任。 

4、增加新的请求处理类很方便。



缺点：

1、不能保证请求一定被接收。

 2、系统性能将受到一定影响，而且在进行代码调试时不太方便，可能会造成循环调用。 

3、可能不容易观察运行时的特征，有碍于除错。