package pers.lipeng.design_pattern.behavior_mode._5_chain_of_responsibility_mode;

class Demo {
    public static class Client{
        public AbstractLogger getChainOfLoggers(){
            AbstractLogger infoLogger = new InfoLogger(AbstractLogger.INFO);
            AbstractLogger debugLogger = new DebugLogger(AbstractLogger.DEBUG);
            AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
            infoLogger.setNextLogger(debugLogger);
            debugLogger.setNextLogger(errorLogger);
            return infoLogger;
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        AbstractLogger chainOfLoggers = client.getChainOfLoggers();

        chainOfLoggers.logMessage(AbstractLogger.INFO,"这是一条INFO日志");
        chainOfLoggers.logMessage(AbstractLogger.DEBUG,"这是一条DEBUG日志");
        chainOfLoggers.logMessage(AbstractLogger.ERROR,"这是一条ERROR日志");
        chainOfLoggers.logMessage(5,"这是一条FATAL日志");
    }
}
