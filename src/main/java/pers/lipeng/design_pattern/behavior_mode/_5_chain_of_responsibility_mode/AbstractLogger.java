package pers.lipeng.design_pattern.behavior_mode._5_chain_of_responsibility_mode;

/**
 * 处理类的抽象类
 */
abstract class AbstractLogger {
    public static final int INFO = 1;
    public static final int DEBUG = 2;
    public static final int ERROR = 3;
    protected int level;
    /**
     * 责任链的下一个元素
     */
    protected AbstractLogger nextLogger;

    public void setNextLogger(AbstractLogger abstractLogger) {
        this.nextLogger = abstractLogger;
    }

    public void logMessage(int level,String message) {
        //如果当前的日志级别等于传入的日志级别，则打印日志，否则交给下一个日志记录器处理
        if(this.level==level){
            write(message);
            return;
        }else if(nextLogger!=null){
            nextLogger.logMessage(level,message);
        }else{
            System.out.println("没有合适的日志记录器");
        }
    }

    abstract void write(String message);


}
