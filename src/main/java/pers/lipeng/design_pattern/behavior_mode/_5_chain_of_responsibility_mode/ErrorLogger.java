package pers.lipeng.design_pattern.behavior_mode._5_chain_of_responsibility_mode;

class ErrorLogger extends AbstractLogger{
    public ErrorLogger(int level){
        this.level = level;
    }
    @Override
    void write(String message) {
        System.out.println("ErrorLogger:"+message);
    }
}
