package pers.lipeng.design_pattern.behavior_mode._8_status_mode;

/**
 * 状态接口
 */
interface State {
    void execute(Context context);
}
