package pers.lipeng.design_pattern.behavior_mode._8_status_mode;

/**
 * 用户对象，环境上下文
 */
class Context {
    private State state;

   public Context(){
       state = null;
   }

    public void setState(State state){
        this.state = state;
    }

    public State getState(){
        return state;
    }
}
