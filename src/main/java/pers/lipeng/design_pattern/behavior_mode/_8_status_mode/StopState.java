package pers.lipeng.design_pattern.behavior_mode._8_status_mode;

class StopState implements State{
    @Override
    public void execute(Context context) {
        System.out.println("stop state");
        context.setState(this);
    }

    @Override
    public String toString(){
        return "stop state";
    }
}
