package pers.lipeng.design_pattern.behavior_mode._8_status_mode;

class StartState implements State{
    @Override
    public void execute(Context context) {
        System.out.println("start state");
        context.setState(this);
    }

    @Override
    public String toString(){
        return "start state";
    }
}
