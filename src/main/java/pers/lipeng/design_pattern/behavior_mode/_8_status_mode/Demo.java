package pers.lipeng.design_pattern.behavior_mode._8_status_mode;

class Demo {
    public static void main(String[] args) {
        // 创建环境
        Context context = new Context();

        //开始状态
        StartState startState = new StartState();
        startState.execute(context);
        System.out.println("context当前状态："+context.getState().toString());


        //停止状态
        StopState stopState = new StopState();
        stopState.execute(context);
        System.out.println("context当前状态："+context.getState().toString());
    }
}
