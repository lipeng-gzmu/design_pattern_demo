package pers.lipeng.design_pattern.behavior_mode._3_observer_mode;

/**
 * 定义被观察者对象
 * 继承主题
 */
class RealObservered extends Subject{
    private String message;

    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
        this.notifyAllObserver(message);
    }
}
