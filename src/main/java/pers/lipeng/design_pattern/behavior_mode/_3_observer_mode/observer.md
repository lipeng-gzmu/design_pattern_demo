# 观察者模式

+ 先讲什么是行为型模型，行为型模式关注的是系统中对象之间的相互交互，解决系统在运行时对象 之间的相互通信和协作，进一步明确对象的职责。
+  观察者模式，是一种行为性模型，又叫发布-订阅模式，他定义对象之间一种一对多的依赖关系， 使得当一个对象改变状态，则所有依赖于它的对象都会得到通知并自动更新。





职责：

观察者模式主要用于1对N的通知。当一个对象的状态变化时，他需要及时告知一系列对象，令他 们做出相应。

  模式：

+  推：每次都会把通知以广播的方式发送给所有观察者，所有的观察者只能被动接收。
+ 拉：观察者只要知道有情况即可，至于什么时候获取内容，获取什么内容，都可以自主决定。





场景：

+ 关联行为场景，需要注意的是，关联行为是可拆分的，而不是“组合”关系。事件多级触发场景。 
+ 跨系统的消息交换场景，如消息队列、事件总线的处理机制。