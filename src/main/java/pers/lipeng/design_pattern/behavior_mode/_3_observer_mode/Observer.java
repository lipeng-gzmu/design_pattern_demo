package pers.lipeng.design_pattern.behavior_mode._3_observer_mode;

/**
 * 观察者
 */
interface Observer {
    /**
     * 观察者方法
     * @param message 传入的消息
     */
    void update(String message);
}
