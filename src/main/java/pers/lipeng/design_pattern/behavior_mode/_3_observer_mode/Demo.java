package pers.lipeng.design_pattern.behavior_mode._3_observer_mode;

class Demo {
    public static void main(String[] args) {
        RealObservered subject =new RealObservered();

        ObserverImpl observer1 = new ObserverImpl();
        ObserverImpl observer2 = new ObserverImpl();
        ObserverImpl observer3 = new ObserverImpl();

        // 注册观察者
        subject.registry(observer1);
        subject.registry(observer2);
        subject.registry(observer3);

        subject.setMessage("我是被观察者，我发生改变了！");
        System.out.println(observer1.getMessage());


        subject.setMessage("我是被观察者，我又发生改变了！");
        System.out.println(observer2.getMessage());
    }
}
