package pers.lipeng.design_pattern.behavior_mode._3_observer_mode;

/**
 * 具体观察者
 */
class ObserverImpl implements Observer{
    /**
     * 具体观察者的属性
     */
    private String message;
    @Override
    public void update(String message) {
        this.message = message;
        System.out.println("观察者接收到消息：" + message);
    }

    public String getMessage(){
        return message;
    }
}
