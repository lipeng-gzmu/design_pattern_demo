package pers.lipeng.design_pattern.behavior_mode._3_observer_mode;

import java.util.Vector;

/**
 * 定义主题
 * 1.定义一个观察者集合
 * 2.定义一个注册观察者的方法
 * 3.定义一个移除观察者的方法
 * 4.定义一个通知所有观察者的方法
 */
class Subject {
    private Vector<Observer> list = new Vector<>();

    /**
     * 注册观察者
     * @param observer
     */
    public void registry(Observer observer){
        list.add(observer);
    }

    /**
     * 移除观察者
     * @param observer
     */
    public void remove(Observer observer){
        list.remove(observer);
    }

    /**
     * 通知所有观察者
     * @param message
     */
    public void notifyAllObserver(String message){
        for (Observer observer : list) {
            observer.update(message);
        }
    }
}
